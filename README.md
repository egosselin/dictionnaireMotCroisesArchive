# Dictionnaire de mots croisés

Est une simple application, que j'ai créée car je ne trouvais pas de dictionnaire de mots croisés simple et/ou sans publicités/autorisations abusives.

Cette application ne requiert pas de connexion à Internet et peu donc être utilisée partout :)

**Prés-requis**
* Android 2.2
* 15 Mo d'espace disque environ

Testé sur Android 2.2 et 4.2.2

## Installation

Importez le projet sous votre IDE préférée, installez une base de données [note à propos de la base de données](https://github.com/egosselin1/dictionnaireMotCroises/blob/master/app/src/main/assets/BDD_LisezMoi.md). Générez l'APK puis copiez-le sur votre appareil.


## Utilisation

Pour rechercher les mots, tapez les lettres connues, puis utilisez la touche ``?`` du clavier virtuel pour insérer un caractére à substituer

** Exemple **

trav____e

* Travaille
* Travelage
* Traversee
* ...

## Compiler et installer l'application

Vous pouvez importer ce projet sur Android studio pour le compiler par vous même, ou bien [télécharger](https://github.com/egosselin1/dictionnaireMotCroises/releases) un apk déjà compilé à installer directement
