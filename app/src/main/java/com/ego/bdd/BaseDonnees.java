package com.ego.bdd;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Ce fichier fait partie du projet Dictionnaire Mots Croisées
 * 
 * Créé par Elvis Gosselin
 * 
 * Voir le README.md pour plus d'informations 
 */

public class BaseDonnees extends SQLiteOpenHelper {
	
    //The Android's default system path of your application database.
    private static String DB_PATH = "/data/data/com.ego.dico/databases/";
    private static String DB_NAME = "dicoMots.png";
 
    private SQLiteDatabase baseDeDonnees; 
 
	private final Context myContext;
	
	//Creation de la BDD
	public BaseDonnees(Context context) {
		super(context, DB_NAME, null, 1);
		this.myContext = context;
	}

	@Override
	public void onCreate(SQLiteDatabase arg0) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onUpgrade(SQLiteDatabase arg0, int arg1, int arg2) {
		// TODO Auto-generated method stub
	}
	
	@Override
	public synchronized void close() {
		if(baseDeDonnees != null) {
			baseDeDonnees.close();
			super.close();
		}	
	}
	
	/**
	 * Créé une base de données vide destinée à être remplacée par notre import
	 * 
	 * @throws IOException
	 */
	public void createDataBase() throws IOException {
 
		boolean bddExiste = verifierBaseDeDonnees();
		
		if(!bddExiste){
			//Création d'un placeholder pour la copie de la base de données
			this.getReadableDatabase();
			
			try {
				//Pour le debug
				//SystemClock.sleep(2500);

				copierBaseDeDonnees();
			} catch (IOException e) {
				throw new Error("Error copying database");
			}
		}
	}
	
	/**
	 * Vérifie que la base de donnéee existe avant de relancer le processus d'import à chaque démarrage
	 * de l'application
	 * 
	 * @return boolean True si la base existe 
	 */
	public boolean verifierBaseDeDonnees() {
		
		SQLiteDatabase checkDB = null;
		
		try {
			String pathBdd = DB_PATH + DB_NAME;
			checkDB = SQLiteDatabase.openDatabase(pathBdd, null, SQLiteDatabase.OPEN_READONLY);
			
			Log.d("DICO", "Bdd trouvée \\o/");
		} catch(SQLiteException e) {
			Log.d("DICO", "Bdd pas trouvée :( ");
		}
		
		if (checkDB != null) {
			checkDB.close();
		}
		
		return checkDB != null ? true : false;
	}
 
	/**
	 * Copies la base de données depuis le dossier des assets vers la base de données vide créée
	 * précédement dans le dossier système (là où il peut-être manipulé) 
	 * 
	 * @throws IOException
	 */
	private void copierBaseDeDonnees() throws IOException {	

		//Ouverture du fichier depuis les assets
		InputStream fichierSource = myContext.getAssets().open(DB_NAME);
		
		//Chemin vers la base de données vide destinée à l'écrasement + ouverture du fichier de destination
		String pathFichierDestination = DB_PATH + DB_NAME;
		OutputStream fichierDestination = new FileOutputStream(pathFichierDestination);
	
		//Stockage et copie via un bytearray
		byte[] buffer = new byte[1024];
	
		int length;
		
		while ((length = fichierSource.read(buffer)) > 0){
			fichierDestination.write(buffer, 0, length);
		}
		
		//Fermeture des flux
		fichierDestination.flush();
		fichierDestination.close();
		fichierSource.close();
	}
	
	/**
	 * Ourvre la base de données existante
	 * 
	 * @throws SQLException
	 */
	public void ouvrirBaseDeDonnees() throws SQLException {	 
		String pathBdd = DB_PATH + DB_NAME;
		baseDeDonnees = SQLiteDatabase.openDatabase(pathBdd, null, SQLiteDatabase.OPEN_READONLY);
	}
}
