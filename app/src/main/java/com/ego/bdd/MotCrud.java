package com.ego.bdd;

import android.content.Context;
import android.database.Cursor;

import java.util.ArrayList;

/**
 * Ce fichier fait partie du projet Dictionnaire Mots Croisées
 * 
 * Créé par Elvis Gosselin
 * 
 * Voir le README.md pour plus d'informations 
 */

public class MotCrud extends BaseDonneesCrud{

	//valeurs par defauts
	private static final String TABLE	 			= "Mots";
	private static final String COL_ID 				= "_id";
	private static final int NUM_COL_ID 			= 0;
	private static final String COL_MOT 			= "Mot";
	private static final int NUM_COL_MOT 			= 1;
	private static final String COL_MOT_PLAT 		= "Mot_Plat";
	private static final int NUM_COL_MOT_PLAT 		= 2;
	private static final String COL_DEFINITION 		= "Definition";
	private static final int NUM_COL_DEFINITION 	= 3;
	
	
	/**
	 * Constructeur, initie le contexte
	 * 
	 * @param context
	 */
	
	public MotCrud(Context context) {
		super(context);
	}

	
	/**
	 * Méthode d'insert
	 * 
	 * @param 		Mot		mot		Un objet mot à insérer	
	 * @return
	 */
	
	public long insert(Mot mot){
		return 0;
	}
	
	
	/**
	 * Méthode d'update
	 * 
	 * @param 		Mot		mot		Un objet mot à insérer	
	 * @return
	 */
	
	public long update(Mot mot){
		return 0;
	}

	
	/**
	 * Selection des mots par LIKE
	 * 
	 * @param 	String				chaine		Un mot partiel
	 * @return	ArrayListe<Mot>				 	Une liste de mots correspondants
	 */
	
	public ArrayList<Mot> selectParChaine(String chaine){
		
		ArrayList<Mot> retour = new ArrayList<Mot>();
		Boolean requeteEstValide = true;
		
		//1er test la chaine est-elle vide?
		if (chaine.length() == 0) {
			requeteEstValide = false;
		}
		
		//2eme test la chaine est-elle constituée de caractères autorisés
		if (!chaine.matches("^[a-z_%]+$")) {
			requeteEstValide = false;
		}
		
		//Si le terme recherché est valide
		if (requeteEstValide) {
			Cursor curseur = sqlite.rawQuery(
				"SELECT * "+
				"FROM "+TABLE+" " +
				"WHERE "+COL_MOT_PLAT+" LIKE '"+chaine+"' "+
				"LIMIT 25",
				new String[]{}
			);
			
			if (curseur.getCount() == 0){
				//Renvoi d'un ArrayList vide
				return retour;		
			}
			
			while(curseur.moveToNext()){
	
				Mot mot = new Mot();
				mot.setId(curseur.getInt(NUM_COL_ID));
				mot.setMot(curseur.getString(NUM_COL_MOT));
				mot.setDefinition(curseur.getString(NUM_COL_DEFINITION));			
				
				retour.add(mot);
			}
		}
		
		return retour;
	}
}
