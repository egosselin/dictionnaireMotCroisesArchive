package com.ego.bdd;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import java.io.IOException;

/**
 * Ce fichier fait partie du projet Dictionnaire Mots Croisées
 * 
 * Créé par Elvis Gosselin
 * 
 * Voir le README.md pour plus d'informations 
 */

public abstract class BaseDonneesCrud {

	protected static SQLiteDatabase sqlite;
	protected BaseDonnees baseDonnees;
	protected Context context;

	/**
	 * Constructeur, prend un objet de contexte en paramètre
	 * 
	 * @param 	Context		context		L'objet du contexte
	 */

	public BaseDonneesCrud(Context context){
		this.context = context;
		//On créer la BDD et sa table
		baseDonnees = new BaseDonnees(this.context);
		
		try {
			baseDonnees.createDataBase();
			
		} catch (IOException ioe) {
			throw new Error("Erreur création BDD");
		}
		
		try {
			baseDonnees.ouvrirBaseDeDonnees();
		} catch(SQLException sqle) {
			throw sqle;
		}
	}
	
	
	/**
	 * public void open()
	 * Hérité de BaseCRUD, ouvre la base de données
	 */
	
	public void open(){
		//on ouvre la BDD en écriture
		sqlite = baseDonnees.getWritableDatabase();
	}
	
	
	/**
	 * public void close()
	 * Ferme la base de données
	 */
	
	public void close(){
		//on ferme l'accès à la BDD
		sqlite.close();
	}
	
	/**
	 * Retourne une instance de la bdd initialisée
	 * 
	 * @return	SQLiteDatabase		Une instance de la bdd chargée
	 */
	
	public SQLiteDatabase getBDD(){
		return sqlite;
	}
}
