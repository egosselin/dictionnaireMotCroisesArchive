package com.ego.bdd;

import java.io.Serializable;

/**
 * Ce fichier fait partie du projet Dictionnaire Mots Croisées
 * 
 * Créé par Elvis Gosselin
 * 
 * Voir le README.md pour plus d'informations 
 */

public class Mot implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private long id;
	private String mot;
	private String definition;
	
	
	/**
	 * Constructeur 
	 * 
	 * @param long id L'id
	 * @param String mot Une chaine comportant le nom 
	 * @param String definition	Une chaine comportant la définition
	 */
	
	public Mot(long id, String mot, String definition){
		this.id = id;
		this.mot = mot;
		this.definition = definition;
	}
	
	
	public Mot(){
		
	}

	
	public long getId() {
		return id;
	}


	public void setId(long id) {
		this.id = id;
	}


	public String getMot() {
		return mot;
	}


	public void setMot(String mot) {
		this.mot = mot;
	}


	public String getDefinition() {
		return definition;
	}


	public void setDefinition(String definition) {
		this.definition = definition;
	}
	
	
	public String toString(){
		return "id : "+this.id+" mot : "+this.mot+" definition : "+this.definition;
	}
}
