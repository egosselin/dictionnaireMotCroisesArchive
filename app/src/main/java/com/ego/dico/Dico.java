package com.ego.dico;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.database.sqlite.SQLiteException;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import com.ego.bdd.Mot;
import com.ego.bdd.MotCrud;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Ce fichier fait partie du projet Dictionnaire Mots Croisées
 * 
 * Créé par Elvis Gosselin
 * 
 * Voir le README.md pour plus d'informations 
 */

public class Dico extends Activity {

	
	protected Context context;
	
	public static Activity activity;
	public static ProgressDialog progress;
	
	public static EditText entree;
	protected ListView sortieListe;
	
	protected Handler rechercheHandler;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		//Set layout
		setContentView(R.layout.activity_dico);
		
		Dico.activity = this;
		progress = new ProgressDialog(this);
		
		
		entree = (EditText)findViewById(R.id.entree);
		sortieListe = (ListView)findViewById(R.id.motstrouves);
		
		//Handler de recherche principal
		rechercheHandler = new Handler(Looper.getMainLooper()) {	
	    	@Override
	        public void handleMessage(Message msg) {
	    		
	    		ArrayList<Mot> resultats = (ArrayList<Mot>) msg.getData().getSerializable("mots");

	    		String sortie = "";
	    		
	    		try{
	    			int i = 0;
	    			
	    			ArrayList<String> mots = new ArrayList<String>();
	    			
	    			for (Mot mot : resultats) {
	    				mots.add(mot.getMot());
	    				i++;
	    			}
	    			
	    			sortie = i+" mot(s) trouvé(s)";
	    
	    			sortieListe.setAdapter(new ArrayAdapter<String>(Dico.activity, R.layout.rangee_simple, mots));
	    		}
	    		catch(NullPointerException e){
	    			//Dans le cas où la requête ne renvoie rien
	    			sortie = "Aucun résultat";
	    		}
	    		
	    		CharSequence texte = sortie;
				
				Toast toast = Toast.makeText(Dico.activity, texte, Toast.LENGTH_SHORT);
				toast.show();
	    		
	    		Log.d("DICO", resultats.toString());
		    }
		};
		
		/**
		 * Ecouteur d'événement au clic sur le bouton rechercher
		 */
		ImageButton nListe = (ImageButton)findViewById(R.id.recherche);
		nListe.setOnClickListener(new View.OnClickListener() {
			
			//Handler du click sur le bouton
			public void onClick(View v) {
				
				Dico.ouvrirMsg("Requête", "Recherche en cours...");
				
				Thread recherche = new Thread(new Runnable() {
					Message message;
					Bundle messageBundle = new Bundle();
					
		            @Override
		            public void run() {
		            	
		            	//Initialisation de la base de données
		            	try{
		            		MotCrud tableMots = new MotCrud(Dico.activity);
		            		
		            		tableMots.open();
		            		ArrayList<Mot> resultats = tableMots.selectParChaine(entree.getText().toString());
		            		tableMots.close();
		            		
		            		//Envoi des résultats au handler
		            		message = rechercheHandler.obtainMessage();
		            		
		            		messageBundle.putSerializable("mots", resultats);
		            		
		            		message.setData(messageBundle);
		            		
		            		rechercheHandler.sendMessage(message);
		            		
		    			}
		    			catch(SQLiteException e){
		    				CharSequence texte = "Erreur BDD";
		    				
		    				Toast toast = Toast.makeText(Dico.activity, texte, Toast.LENGTH_SHORT);
		    				toast.show();
		    			}
		            	
		            	//Fermeture du message
		            	Dico.fermerMsg();
		            }
				});
				
				//Démarrage du thread
				recherche.start();
			}
		});
		
		ToucheClavierFactory toucheClavierFactory = new ToucheClavierFactory(Dico.activity, entree);

		try {
			View bta = toucheClavierFactory.mapperTouche("a");
			View btb = toucheClavierFactory.mapperTouche("b");
			View btc = toucheClavierFactory.mapperTouche("c");
			View btd = toucheClavierFactory.mapperTouche("d");
			View bte = toucheClavierFactory.mapperTouche("e");
			View btf = toucheClavierFactory.mapperTouche("f");
			View btg = toucheClavierFactory.mapperTouche("g");
			View bth = toucheClavierFactory.mapperTouche("h");
			View bti = toucheClavierFactory.mapperTouche("i");
			View btj = toucheClavierFactory.mapperTouche("j");
			View btk = toucheClavierFactory.mapperTouche("k");
			View btl = toucheClavierFactory.mapperTouche("l");
			View btm = toucheClavierFactory.mapperTouche("m");
			View btn = toucheClavierFactory.mapperTouche("n");
			View bto = toucheClavierFactory.mapperTouche("o");
			View btp = toucheClavierFactory.mapperTouche("p");
			View btq = toucheClavierFactory.mapperTouche("q");
			View btr = toucheClavierFactory.mapperTouche("r");
			View bts = toucheClavierFactory.mapperTouche("s");
			View btt = toucheClavierFactory.mapperTouche("t");
			View btu = toucheClavierFactory.mapperTouche("u");
			View btv = toucheClavierFactory.mapperTouche("v");
			View btw = toucheClavierFactory.mapperTouche("w");
			View btx = toucheClavierFactory.mapperTouche("x");
			View bty = toucheClavierFactory.mapperTouche("y");
			View btz = toucheClavierFactory.mapperTouche("z");

			View btSpecial = toucheClavierFactory.mapperTouche("special");
			View btEffacer = toucheClavierFactory.mapperTouche("effacer");
			
		} catch (IOException e) {
			Toast toast = Toast.makeText(Dico.activity, e.getMessage(), Toast.LENGTH_SHORT);
			toast.show();
		}
		
	}
	
	@Override
	protected void onStart() {
		super.onStart();
        
        //Ouverture du message d'initialisation
		Dico.ouvrirMsg("Initialisation", "Création de la base de données");
        
		//Creation de la base de données dans un simple thread séparé
        new Thread((new Runnable() {
            @Override
            public void run() {
            	//Initialisation de la base de données
            	MotCrud motCrud = new MotCrud(Dico.activity);
            	
          		motCrud = new MotCrud(Dico.activity);
          		motCrud.open();
          		
          		//Fermeture du message
          		Dico.fermerMsg();
            }
        })).start();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_dico, menu);
		return true;
	}
	
	/**
	 * Méthode statique d'affichage de message
	 * 
	 * @param titre Le titre du message
	 * @param message Le contenu du message
	 */
	public static void ouvrirMsg(String titre, String message) {
		Log.d("DICO", "Ouverture message");
		progress.setTitle(titre);
		progress.setMessage(message);
		progress.setIndeterminate(true);
		progress.show();
	}
	
	/**
	 * Méthode statique de fermeture de message
	 */
	public static void fermerMsg() {
		Log.d("DICO", "Fermeture message");
		progress.dismiss();
	}
}
