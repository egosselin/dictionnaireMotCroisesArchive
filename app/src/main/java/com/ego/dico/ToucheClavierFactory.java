package com.ego.dico;

import android.app.Activity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import java.io.IOException;

public class ToucheClavierFactory {

	protected Activity activity;
	protected EditText sortie;
	protected View touche;
	
	public ToucheClavierFactory(Activity activity, EditText sortie) {
		this.activity = activity;
		this.sortie = sortie;
	}
	
	public View mapperTouche(final String lettre) throws IOException {
		
		if (lettre.equals("a")) {
			this.touche = (Button)activity.findViewById(R.id.clavierA);
		} else if(lettre.equals("b")) {
			this.touche = (Button)activity.findViewById(R.id.clavierB);
		} else if(lettre.equals("c")) {
			this.touche = (Button)activity.findViewById(R.id.clavierC);
		} else if(lettre.equals("d")) {
			this.touche = (Button)activity.findViewById(R.id.clavierD);
		} else if(lettre.equals("e")) {
			this.touche = (Button)activity.findViewById(R.id.clavierE);
		} else if(lettre.equals("f")) {
			this.touche = (Button)activity.findViewById(R.id.clavierF);
		} else if(lettre.equals("g")) {
			this.touche = (Button)activity.findViewById(R.id.clavierG);
		} else if(lettre.equals("h")) {
			this.touche = (Button)activity.findViewById(R.id.clavierH);
		} else if(lettre.equals("i")) {
			this.touche = (Button)activity.findViewById(R.id.clavierI);
		} else if(lettre.equals("j")) {
			this.touche = (Button)activity.findViewById(R.id.clavierJ);
		} else if(lettre.equals("k")) {
			this.touche = (Button)activity.findViewById(R.id.clavierK);
		} else if(lettre.equals("l")) {
			this.touche = (Button)activity.findViewById(R.id.clavierL);
		} else if(lettre.equals("m")) {
			this.touche = (Button)activity.findViewById(R.id.clavierM);
		} else if(lettre.equals("n")) {
			this.touche = (Button)activity.findViewById(R.id.clavierN);
		} else if(lettre.equals("o")) {
			this.touche = (Button)activity.findViewById(R.id.clavierO);
		} else if(lettre.equals("p")) {
			this.touche = (Button)activity.findViewById(R.id.clavierP);
		} else if(lettre.equals("q")) {
			this.touche = (Button)activity.findViewById(R.id.clavierQ);
		} else if(lettre.equals("r")) {
			this.touche = (Button)activity.findViewById(R.id.clavierR);
		} else if(lettre.equals("s")) {
			this.touche = (Button)activity.findViewById(R.id.clavierS);
		} else if(lettre.equals("t")) {
			this.touche = (Button)activity.findViewById(R.id.clavierT);
		} else if(lettre.equals("u")) {
			this.touche = (Button)activity.findViewById(R.id.clavierU);
		} else if(lettre.equals("v")) {
			this.touche = (Button)activity.findViewById(R.id.clavierV);
		} else if(lettre.equals("w")) {
			this.touche = (Button)activity.findViewById(R.id.clavierW);
		} else if(lettre.equals("x")) {
			this.touche = (Button)activity.findViewById(R.id.clavierX);
		} else if(lettre.equals("y")) {
			this.touche = (Button)activity.findViewById(R.id.clavierY);
		} else if(lettre.equals("z")) {
			this.touche = (Button)activity.findViewById(R.id.clavierZ);
		} else if(lettre.equals("special")) {
			this.touche = (ImageButton)activity.findViewById(R.id.clavierSpecial);
		} else if(lettre.equals("effacer")) {
			this.touche = (ImageButton)activity.findViewById(R.id.clavierEffacer);
		} else {
			throw new IOException("touche inconnue : "+lettre);
		}
		
		this.touche.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				if (lettre.equals("special")) {
					//Caractère inconnu
					sortie.setText(sortie.getText().toString() + "_");
				} else if (lettre.equals("effacer")) {
					//Effacement
					sortie.setText(retirerChar(sortie.getText().toString()));
				} else {
					sortie.setText(sortie.getText().toString() + lettre);
				}
			}
		});
		
		return this.touche;
	}

	private String retirerChar(String str) {
		if (str != null && str.length() > 0) {
			str = str.substring(0, str.length()-1);
		}
		return str;
	}
}