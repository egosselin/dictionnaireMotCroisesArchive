# Informations relatives à la base de données

La base de données est extraite du projet GPL [Français - Gutemberg](http://www.fifi.org/doc/ifrench-gut/fr/ALIRE)

Vous pouvez la personaliser en utilisant la structure suivante

```sql
CREATE TABLE `Mots` (
	`_id`	INTEGER NOT NULL DEFAULT (null),
	`Mot`	TEXT NOT NULL,
	`Definition`	TEXT,
	PRIMARY KEY(_id)
);
```

Nom du fichier : dicoMots.png

## Comptatibilité

La base de données doit utiliser une extension au format **.png**, j'ai utilisé ce hack peu élégant pour conserver une compatibilité avec la version 2.2 d'Android.

Il faut savoir que pour les version d'Android < 2.3, la taille maximale d'un fichier importable (dump sqlite) ne dois pas exéder 1024Ko dans le cas d'un fichier "compressible", d'où l'utilisation de ce format, le png étant considéré comme non compressible.

Plus d'infos sur [sur ce blog](http://ponystyle.com/blog/2010/03/26/dealing-with-asset-compression-in-android-apps/)